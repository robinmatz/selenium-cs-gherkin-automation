﻿using NUnit.Framework;
using Selenium_CS_Gherkin_Automation.Pages;
using TechTalk.SpecFlow;

namespace Selenium_CS_Gherkin_Automation.Steps
{
    [Binding]
    public class FormsSteps
    {
        private readonly DriverHelper driverHelper;
        private readonly HomePage homePage;
        private readonly FormsPage formsPage;

        public FormsSteps(DriverHelper driverHelper)
        {
            this.driverHelper = driverHelper;
            homePage = new HomePage(this.driverHelper.Driver);
            formsPage = new FormsPage(this.driverHelper.Driver);
        }

        [Given(@"I navigate to the home page")]
        public void GivenINavigateToTheHomePage()
        {
            homePage.NavigateToHomePage();
        }
        
        [Given(@"I navigate to the forms page")]
        public void GivenINavigateToTheFormsPage()
        {
            homePage.NavigateToFormsPage();
        }
        
        [Given(@"I select the Practice Form")]
        public void GivenISelectThePracticeForm()
        {
            formsPage.SelectPracticeForm();
        }
        
        [Given(@"I enter my details into the form")]
        public void GivenIEnterMyDetailsIntoTheForm()
        {
            formsPage.EnterPersonalDetails();
        }
        
        [Given(@"I click submit")]
        public void GivenIClickSubmit()
        {
            formsPage.Submit();
        }
        
        [Then(@"the practice form should be displayed")]
        public void ThenThePracticeFormShouldBeDisplayed()
        {
            formsPage.VerifyPracticeFormDisplayed();
        }
        
        [Then(@"a success message should appear")]
        public void ThenASuccessMessageShouldAppear()
        {
            formsPage.VerifySuccessMessageDisplayed();
        }
        
        [Then(@"the details should match the previously entered details")]
        public void ThenTheDetailsShouldMatchThePreviouslyEnteredDetails()
        {
            formsPage.DetailsMatch();
        }
    }
}
