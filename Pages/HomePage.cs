﻿using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Selenium_CS_Gherkin_Automation.Pages
{
    [Binding]
    class HomePage
    {
        #region
        private readonly IWebDriver driver;

        IJavaScriptExecutor Executor => (IJavaScriptExecutor)driver;

        readonly string homePageUrl = "https://demoqa.com/";
        
        readonly string homePageTitle = "ToolsQA";

        IWebElement LblElements => driver.FindElement(By.XPath("//*[text()='Elements']"));

        IWebElement LblForms => driver.FindElement(By.XPath("//*[text()='Forms']"));

        IWebElement LblAlertFrameWindows => driver.FindElement(By.XPath("//*[text()='Alerts, Frame & Windows']"));

        IWebElement LblWidgets => driver.FindElement(By.XPath("//*[text()='Widgets']"));

        IWebElement LblInteractions => driver.FindElement(By.XPath("//*[text()='Interactions']"));

        IWebElement LblBookStoreApplication => driver.FindElement(By.XPath("//*[text()='Book Store Application']"));
        #endregion

        #region
        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        internal void NavigateToHomePage()
        {
            driver.Navigate().GoToUrl(homePageUrl);
            Assert.That(driver.Title.Equals(homePageTitle));
        }

        internal void NavigateToFormsPage()
        {
            Executor.ExecuteScript("arguments[0].scrollIntoView(true);", LblForms);
            LblForms.Click();
        }
        #endregion
    }
}
