﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace Selenium_CS_Gherkin_Automation
{
    [Binding]
    class FormsPage
    {
        #region
        private readonly IWebDriver driver;

        IJavaScriptExecutor Executor => (IJavaScriptExecutor)driver;

        IWebElement LblPracticeForm => driver.FindElement(By.XPath("//*[text()='Practice Form']"));

        IWebElement HdrForm => driver.FindElement(By.XPath("//*[text()='Student Registration Form']"));

        IWebElement TxtFirstName => driver.FindElement(By.Id("firstName"));

        IWebElement TxtLastName => driver.FindElement(By.Id("lastName"));

        IWebElement TxtEmail => driver.FindElement(By.Id("userEmail"));

        IWebElement RdoMale => driver.FindElement(By.XPath("//label[@for='gender-radio-1']"));

        IWebElement RdoFemale => driver.FindElement(By.XPath("//label[@for='gender-radio-2']"));

        IWebElement RdoOther => driver.FindElement(By.XPath("//label[@for='gender-radio-3']"));

        IWebElement TxtMobileNumber => driver.FindElement(By.Id("userNumber"));

        #region
        IWebElement TxtDateOfBirth => driver.FindElement(By.Id("dateOfBirthInput"));

        IWebElement DdlMonth => driver.FindElement(By.ClassName("react-datepicker__month-select"));

        IWebElement OptFebruary => driver.FindElement(By.XPath("//select[@class='react-datepicker__month-select']/option[@value='1']"));

        IWebElement DdlYear => driver.FindElement(By.ClassName("react-datepicker__year-select"));

        IWebElement Opt1983 => driver.FindElement(By.XPath("//select[@class='react-datepicker__year-select']/option[@value='1983']"));

        IWebElement TxtDay => driver.FindElement(By.XPath("(//*[@class='react-datepicker-popper']//*[text()='1'])[1]"));
        #endregion

        IWebElement TxtSubject => driver.FindElement(By.XPath("//*[contains(@class, 'subjects')]"));

        IWebElement ChkSports => driver.FindElement(By.XPath("//label[@for='hobbies-checkbox-1']"));

        IWebElement ChkReading => driver.FindElement(By.XPath("//label[@for='hobbies-checkbox-2']"));

        IWebElement ChkMusic => driver.FindElement(By.XPath("//label[@for='hobbies-checkbox-3']"));

        IWebElement TxtAddress => driver.FindElement(By.Id("currentAddress"));

        IWebElement DdlState => driver.FindElement(By.XPath("//*[text()='Select State']"));
        
        IWebElement DdlCity => driver.FindElement(By.XPath("//*[text()='Select City']"));

        IWebElement BtnSubmit => driver.FindElement(By.Id("submit"));

        IWebElement HdrSuccess => driver.FindElement(By.XPath("//*[@id='example-modal-sizes-title-lg']"));

        IWebElement TxtStudentName => driver.FindElement(By.XPath("//*[text()='Student Name']/../td[2]"));

        IWebElement TxtStudentEmail => driver.FindElement(By.XPath("//*[text()='Student Email']/../td[2]"));

        IWebElement TxtGender => driver.FindElement(By.XPath("//*[text()='Gender']/../td[2]"));

        IWebElement TxtMobile => driver.FindElement(By.XPath("//*[text()='Mobile']/../td[2]"));

        IWebElement TxtDateOfBirthForm => driver.FindElement(By.XPath("//*[text()='Date of Birth']/../td[2]"));
        
        IWebElement TxtSubjects => driver.FindElement(By.XPath("//*[text()='Subjects']/../td[2]"));

        IWebElement TxtHobbies => driver.FindElement(By.XPath("//*[text()='Hobbies']/../td[2]"));
        
        IWebElement TxtAddressForm => driver.FindElement(By.XPath("//*[text()='Address']/../td[2]"));
        
        IWebElement TxtCityAndState => driver.FindElement(By.XPath("//*[text()='State and City']/../td[2]"));
      
        #endregion

        #region
        public FormsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        internal void SelectPracticeForm()
        {
            LblPracticeForm.Click();
        }

        internal void EnterPersonalDetails()
        {
            TxtFirstName.SendKeys("Peter");
            TxtLastName.SendKeys("Meier");
            TxtEmail.SendKeys("peter.meier@example.com");
            RdoMale.Click();
            TxtMobileNumber.SendKeys("1234567890");
            
            TxtDateOfBirth.Click();
            DdlMonth.Click();
            OptFebruary.Click();
            DdlYear.Click();
            Opt1983.Click();
            TxtDay.Click();
                       
            ChkMusic.Click();
            ChkReading.Click();
            ChkSports.Click();

            TxtAddress.SendKeys("Sample Street 1");

         }

        internal void Submit()
        {
            Executor.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            BtnSubmit.Click();
        }

        internal void VerifyPracticeFormDisplayed()
        {
            Assert.That(HdrForm.Displayed);
        }

        internal void VerifySuccessMessageDisplayed()
        {
            Assert.That(HdrSuccess.Displayed);
        }

        internal void DetailsMatch()
        {
            Assert.AreEqual("Peter Meier", TxtStudentName.Text);
            Assert.AreEqual("peter.meier@example.com", TxtStudentEmail.Text);
            Assert.AreEqual("Male", TxtGender.Text);
            Assert.AreEqual("1234567890", TxtMobile.Text);
            Assert.AreEqual("01 February,1983", TxtDateOfBirthForm.Text);
            Assert.AreEqual("Music, Reading, Sports", TxtHobbies.Text);
        }
        #endregion
    }
}