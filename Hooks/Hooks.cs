﻿using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Selenium_CS_Gherkin_Automation.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private readonly DriverHelper driverHelper;

        public Hooks(DriverHelper driverHelper)
        {
            this.driverHelper = driverHelper;
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");

            driverHelper.Driver = new ChromeDriver(options);
        }

        [AfterScenario]
        public void AfterScenario()
        {
            if ( driverHelper.Driver != null )
            {
                driverHelper.Driver.Quit();
            }
        }
    }
}
