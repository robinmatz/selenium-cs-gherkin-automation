﻿Feature: Forms
	Filling out student registration form

@smoke
Scenario: Fill out form with valid data
	Given I navigate to the home page
	And I navigate to the forms page
	And I select the Practice Form
	Then the practice form should be displayed

	Given I enter my details into the form
	And I click submit
	Then a success message should appear
	And the details should match the previously entered details